package com.dr.leo.powerleo.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author leo
 */
@SpringBootApplication
public class PowerleoApplication {

    public static void main(String[] args) {
        SpringApplication.run(PowerleoApplication.class, args);
    }

}
